package mbsd.pressureproject;

/**
 * Created by Stephen Standaloft on 15/11/2015.
 */

import android.util.Log;
/**
 * Created by Stevo on 15/11/2015.
 */
import java.text.*;

class AssignedPhrase extends Phrase{

    // stores a given phrase and stats pertaining to the phrase
    // and the performance of the participant in terms of speed/errors

    private float duration; // typing time for phrase in miliseconds
    private int keyStrokes; // total number of typed keystrokes

    private float cps; // characters per second
    private float wpm; // words per minute
    private float kspc;  // keystrokes per character
    private int editDist; // edit distance between assigned/input phrase

    AssignedPhrase(Phrase p) {
        super(p.getPhrase());

        duration=0;
        keyStrokes=0;

        cps = 0;
        wpm = 0;
        kspc = 0;
        editDist = 0;
    }

    protected void setStats(float dur, int ks, String input) {
        // reads in the duration and keystokes for a given phrase along with phrase input
        // and uses them to generate the statistics

        duration = dur;
        keyStrokes=ks;

        // cps = chararacter count/duration
        cps = (super.getChrCount()*1000)/duration; // duration needs converting into seconds
        wpm = cps*60/5; // standard typist definition of wpm: 5 char words
        kspc = keyStrokes/super.getChrCount(); // total keystrokes divided by total characters
        editDist = computeEditDist(input); // compute edit distance


    }

    private int computeEditDist(String s) {
        // returns the edit distance between this phrase and string s

        int x = getPhrase().length()+1;
        int y = s.length()+1;
        int[][] d = new int[x][y]; // distance matrix

        // initialise distance matrix
        // distance between this phrase and empty string down rows
        for (int i = 0; i < x; i++)
            d[i][0]=i;
        // distance between string s and empty string along columns
        for (int i = 0; i < y; i++)
            d[0][i]=i;

        for (int i = 1; i < x; i++) {
            for (int j = 1; j < y; j++) {



                // if current chars match use distance for phrase substring 0..i-1 and s substring 0..j-1
                if(getPhrase().charAt(i-1)==s.charAt(j-1)) {
                    d[i][j]=d[i-1][j-1];
                }
                // otherwise take minimum of distance between s substring and phrase,
                // phrase substring and s or sustrings for both 0..i-1 and 0..j-1
                else {
                    d[i][j]=1+min3(d[i][j-1],d[i-1][j],d[i-1][j-1]);
                }
            }
        }

        return d[x-1][y-1];
    }

    protected int min3 (int x, int y, int z) {
        // returns the minimum value of three ints x, y, z
        if (x<y && x<z)
            return x;
        else if (y<z)
            return y;
        else
            return z;
    }

    protected float getCPS() {
        return cps;
    }

    protected float getWPM() {
        return wpm;
    }

    protected float getKSPC() {
        return kspc;
    }

    protected String getStats() {
        // returns the statistics for this assigned phrase

        String out = super.getStats()+",";

        DecimalFormat df = new DecimalFormat("#.##");
        df.setMinimumFractionDigits(2);

        out+=df.format(duration/1000)+","+keyStrokes+","
                +df.format(cps) + "," + df.format(wpm) + "," + df.format(kspc)+","+editDist;

        return out;
    }

} //AssignedPhrase