package mbsd.pressureproject;

/**
 * Created by Stephen Standaloft on 15/11/2015.
 */

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.*;
import java.util.*;

class SpeedTrial {

    // control class for the speed trial which consists of two SpeedTests
    // the two tests will use either standard or pressure input
    // writes out the phrases/stats to a file once both tests are completed

    static private File phraseSet, websites; // input files
    static private PhraseGenerator phraseGenerator; // generates random phrases

    protected static final String STATS_TABLE_HEADER = "Subject,Pressure,Phrase,Words,Chars,Caps,Digits,Symbols,"
            +"Duration(s),Keys,CPS,WPM,KSPC,EditDist\n";  // table header

    private ArrayList<SpeedTest> trial; // array list of trial data
    private String participantName;  // name of participant

    protected SpeedTrial(String partName) {

        // see if input files need to be read in or not
        if (phraseSet == null || websites == null) {
            
            // gain access to files
            phraseSet = new File(Environment.getExternalStorageDirectory(), "pressureproject/phraseSet.txt");
            websites = new File(Environment.getExternalStorageDirectory(), "pressureproject/websites.txt");

            // pass files to the static phraseGenerator
            try {
                phraseGenerator = new PhraseGenerator(phraseSet, websites);
            } catch (IOException ie) { Log.e("file error", ie.getMessage()); }

        }

        trial = new ArrayList<SpeedTest>();
        participantName=partName;
    }

    protected SpeedTest nextTest(boolean usePressure) {
        // returns the phrases and stats for the next test
        SpeedTest test = new SpeedTest(usePressure,participantName);
        trial.add(test);
        return test;
    }

    protected String getStats() {
        // prints out stats for each test in the trial
        // stats table header included
        String out = STATS_TABLE_HEADER;

        for (SpeedTest test:trial) {
            out+=test.exportStats();
        }
        return out;
    }

    protected String getPhrases() {
        // prints out phrases for each test in the trial
        String out = "";

        for (SpeedTest test:trial) {
            out+=test.exportPhrases();
        }
        return out;
    }


    protected void exportToFile() {
        // exports all data to file

        File stats = new File(Environment.getExternalStorageDirectory(),
                  "pressureproject/subject " + participantName +".csv");

        try {

            FileOutputStream fos = new FileOutputStream(stats);
            OutputStreamWriter osw = new OutputStreamWriter(fos);

            // output stats
            osw.write(getStats());
            // new line seperators
            osw.write("\n\n\n");
            // output phrases
            osw.write(getPhrases());
            // close the file
            osw.close();

        } catch (IOException ie) { Log.e("file error",ie.getMessage()); }

    }

    public String toString() {
        // outputs the stats/phrases for this SpeedTrial
        return getStats()+"\n\n\n"+getPhrases();
    }

    public boolean trialComplete() {
        return trial.size()==2;
    }

    protected static PhraseGenerator getPhraseGenerator() {
        return phraseGenerator;
    }

} // SpeedTrial