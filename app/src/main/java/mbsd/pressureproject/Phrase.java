package mbsd.pressureproject;

/**
 * Created by Stephen Standaloft on 15/11/2015.
 */

class Phrase{

    // stores a given phrase and its stats

    private String[] word; // array of words for a given phrase

    private int wrdCount; // number of words in a phrase
    private int chrCount; // number of characters in a phrase
    private int capCount; // number of capital letters in a phrase
    private int digCount; // number of digits in a phrase
    private int symCount; // number of symbols in a phrase

    protected Phrase(String line) {

        word = line.split(" "); // generate array of words, filtering out whitespace

        wrdCount=word.length; // number of words in phrase
        chrCount=line.length()-wrdCount+1; // number of characters in the words (ignoring spaces)
        // number of spaces between words is wrdCount-1
        // i.e. changing signs adjust chrCount by -wrdCount+1

        capCount=findCaps(line); // number of caps in phrase
        digCount=findDigs(line); // number of digits in phrase
        symCount=findSyms(line); // number of symbols in phrase
    }

    private int findCaps(String line) {
    // returns the number of capital letters in this phrase
        capCount=0;
        for(int i = 0; i < line.length(); i++)
            if(Character.isUpperCase(line.charAt(i)))
                capCount++;
        return capCount;
    }

    private int findDigs(String line) {
    // returns the number of digits in this phrase

        digCount=0;
        for(int i = 0; i < line.length(); i++)
            if(Character.isDigit(line.charAt(i))) // if first letter of word is digit the rest must be
                digCount++;
        return digCount;

    }

    private int findSyms(String line) {
    // returns the number of symbols in this phrase

        symCount=0;
        for(int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            if(!Character.isLetterOrDigit(c)&& c!=' ')
                symCount++;
        }
        return symCount;
    }

    protected Phrase titleCase() {
        // returns a copy of this phrase in titleCase
        Phrase p = new Phrase(this.toString());
        for (int i = 0; i < p.word.length; i++) {
            char c = Character.toUpperCase(p.word[i].charAt(0));
            p.word[i]=c+p.word[i].substring(1);   //make first letter of each word uppercase
            p.capCount++;
        }
        return p;
    }

    protected String getStats() {
        // returns the statistics for this phrase
        String out = "";

        out = wrdCount + "," + chrCount + "," + capCount
                + "," + digCount + "," + symCount;

        return out;
    }

    protected int getWrdCount() {
        return wrdCount;
    }

    protected int getChrCount() {
        return chrCount;
    }

    protected int getCapCount() {
        return capCount;
    }

    protected int getDigCount() {
        return digCount;
    }

    protected int getSymCount() {
        return symCount;
    }

    protected String getPhrase() {
        // maps this phrase from an array of words to a string of phrase
        String phrase="";
        for (int i = 0; i < word.length-1; i++)
            phrase+=word[i]+" ";
        return phrase+word[word.length-1];
    }

    public String toString() {
        return getPhrase();
    }

    public boolean equals(Object o) {
        // set equality based on whether same phrase is represented
        if (o==null) return false;
        if (o==this) return true;
        Phrase p = (Phrase)o;

        return getPhrase().equals(p.getPhrase());
    }

} //Phrase
