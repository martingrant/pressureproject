package mbsd.pressureproject;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import android.os.Looper;

public class ShakeDeviceCS6 {
	private BluetoothSocket btsock = null;
	private InputStream is = null;
	private OutputStream os = null;
	
	private double FSR0;
	private double prevFSR0 = 0.0;
	private Object FSR0_Lock = new Object();
	private double FSR1;
	private double prevFSR1;
	private Object FSR1_Lock = new Object();
	private double FSR2;
	private double prevFSR2;
	private Object FSR2_Lock = new Object();
	private double FSR3;
	private double prevFSR3;
	private Object FSR3_Lock = new Object();
	
	
	/**
	 * MAX_PRESSURE
	 */
	private static double MAX_PRESSURE = 570000.0;
	//private static double MAX_PRESSURE = 999999999;
	
	

	
	
	
	public double FSR0()
	{
		double temp;
		synchronized (FSR0_Lock) {
			temp = FSR0;
		}
		return temp;
	}
	
	public double FSR1()
	{
		double temp;
		synchronized (FSR1_Lock) {
			temp = FSR1;
		}
		return temp;
	}
	public double FSR2()
	{
		double temp;
		synchronized (FSR2_Lock) {
			temp = FSR2;
		}
		return temp;
	}
	public double FSR3()
	{
		double temp;
		synchronized (FSR3_Lock) {
			temp = FSR3;
		}
		return temp;
	}

	
	
	public boolean openConnection(String btaddress)
	{

		BluetoothAdapter adapt = BluetoothAdapter.getDefaultAdapter();
		BluetoothDevice btdev = null;
		Set<BluetoothDevice> pairedDevices = adapt.getBondedDevices();
		// If there are paired devices
		if (pairedDevices.size() > 0) {
		    // Loop through paired devices
		    for (BluetoothDevice device : pairedDevices) {
		    	//need a way to check the address here
		    	//address is 00:50:C2:A1:D2:07
	        	btdev = device;

		        
		    }

		}

		if(btdev == null)
		{
	        return false;
		}
		
		
		try {
			btsock = btdev.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
			
		} catch (IOException e) {
			Log.v("SHAKE", e.toString());
			return false;
		}
		

		try {
			btsock.connect();
			os = btsock.getOutputStream();
			is = btsock.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		try {
			//here, we set some settings on the shake board. See documentation.
			//byte[] w = {'$','$','W',0x00,0x00,0x00}; //0x00 for binary packets 0x01 for ASCII
			//os.write(w);
			//byte[] w4 = {'$','$','W',0x01,0x00,0x01}; //set to conductance. 
			//os.write(w4);
			//byte[] w1 = {'$','$','W',0x03,0x00,0x02}; //if using ACSII packets, 0x02 for FSRs
			//os.write(w1);
			//byte[] w2 = {'$','$','W',0x05,0x00,0x03}; //0x00-0x03 for FSR channels. 
			//os.write(w2);
			//os.flush();
			
			byte[] w = {'$','$','W',0x00,0x00,0x00}; //0x00 for binary packets 0x01 for ASCII
			os.write(w);
			byte[] w4 = {'$','$','W',0x01,0x00,0x01}; //set to conductance. 
			os.write(w4);
			//byte[] w1 = {'$','$','W',0x03,0x00,0x02}; //if using ACSII packets, 0x02 for FSRs
			//os.write(w1);
			//byte[] w2 = {'$','$','W',0x05,0x00,0x03}; //0x00-0x03 for FSR channels. 
			//os.write(w2);
			os.flush();
			
			//Log.v("SHAKE", "ASCII SET?");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Thread newThread = new Thread(new ReadThread());
		//newThread.setPriority(Thread.MAX_PRIORITY);
		newThread.start();

		return true;
	}
	

	public boolean closeConnection() {
		try {
			is.close();
			os.close();
			btsock.close();
		} catch (IOException ioe) {}
		return false;
	}
	
	class ReadThread implements Runnable
	{

		@Override
		public void run() {
			String s = "";
			byte[] readbuf = new byte[70];
			int readcount = 0;

			while(true){

				try {

					int n = is.read();
					if(n == '$' || n == 'D')
					{
						if(s.length() > 3)
						{
							try
							{
								//convert from bytes to uint32
								//low pass filter.
								double tempFSR0 = ((readbuf[59]&0xFF) << 24) |
									((readbuf[58]&0xFF) << 16) |
									((readbuf[57]&0xFF) << 8)  |
									(readbuf[56]&0xFF);
								//double tempFSR0 = prevFSR0 + 0.1 *(FSR_0 - prevFSR0);
								//prevFSR0 = tempFSR0;
								//Log.v("SHAKE", ""+tempFSR0);
								
								
								synchronized (FSR0_Lock) {
									if(tempFSR0>MAX_PRESSURE)
										tempFSR0=MAX_PRESSURE;
									if(tempFSR0 < 0)
										tempFSR0 = 0;
									FSR0 = ((tempFSR0/MAX_PRESSURE)*100.0);
								}
								
								long FSR_1	= ((readbuf[55]&0xFF) << 24) | 
						              	((readbuf[54]&0xFF) << 16) | 
						              ((readbuf[53]&0xFF) << 8)  | 
						               (readbuf[52]&0xFF);
								//low pass filter.
								//double tempFSR1 = prevFSR1 + 0.01 *(FSR_1 - prevFSR1);
								//prevFSR1 = tempFSR1;
								
								synchronized (FSR1_Lock) {
									FSR1 = ((FSR_1/MAX_PRESSURE)*100.0);
									//FSR1 = FSR1*1;
								}
								
								long FSR_2	= ((readbuf[63]&0xFF) << 24) | 
						              	((readbuf[62]&0xFF) << 16) | 
						              ((readbuf[61]&0xFF) << 8)  | 
						               (readbuf[60]&0xFF);
								
								synchronized (FSR2_Lock) {
									FSR2 = ((FSR_2/MAX_PRESSURE)*100.0);
									//FSR2 = FSR2*1;
								}
								
								long FSR_3	= ((readbuf[67]&0xFF) << 24) | 
						              	((readbuf[66]&0xFF) << 16) | 
						              ((readbuf[65]&0xFF) << 8)  | 
						               (readbuf[64]&0xFF);
								
								synchronized (FSR3_Lock) {
									FSR3 = ((FSR_3/MAX_PRESSURE)*100.0);
									//FSR3 = FSR3*1;
								}
								
							}	
							catch(Exception e)
							{
								//nothing to be done. Just ignore.
							}

							s="";
							readcount = 0;
							readbuf = new byte[70];
							s += (char) n;
							readbuf[readcount++] = (byte) n;
						}
						else
						{
							s +=(char) n;
							readbuf[readcount++] = (byte) n;
						}
						
					}
					else
					{
						s += "," + n;
						readbuf[readcount++] = (byte) n;
					}
					
					

				} catch (IOException e) {
					//Log.v("SHAKE","ERROR_IO");
				}
				catch (Exception e)
				{
					//Log.v("SHAKE","ERROR_OTHER");
			
				}
			}
			
		}
		
		
	}
	
	
}
