package mbsd.pressureproject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import android.os.CountDownTimer;

import java.util.*;

public class MainActivity extends AppCompatActivity {

    private TextView phraseText;
    private static EditText editText;
    private Button submitButton;
    private Button nextTrialButton;
    private int phraseCounter = 0;
    private long startTime;
    private static boolean trialStarted = true;

    // manage stats for speed trial
    SpeedTrial speedTrial;

    ArrayList<AssignedPhrase> phrasesList;

    SpeedTest test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Set up GUI
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Get the on screen text elements for later manipulation
        phraseText = (TextView) findViewById(R.id.textView);
        editText = (EditText) findViewById(R.id.editText);

        //Get the on screen buttons for later manipulation
        submitButton = (Button) findViewById(R.id.doneButton);
        nextTrialButton = (Button) findViewById(R.id.nextTrialButton);

        //Start the trial
        newSpeedTrial(getIntent().getStringExtra("participantName"));

    }

    //Start a new speed trial
    void newSpeedTrial(String participantName) {

        // set up stats collector for the speed trial
        speedTrial = new SpeedTrial(participantName);

        //Start a new test
        setListeners();
        newTest();

    }

    void secondTest() {

        //Hide the next trial button and show the submit phrase button
        submitButton.setVisibility(View.VISIBLE);
        nextTrialButton.setVisibility(View.INVISIBLE);

        //Start a new test
        newTest();

    }

    //Start a new test
    void newTest() {


        trialStarted = false;

        //Delete any text from the phrases text box
        changeText(phraseText, "");

        //Start a new test based on if pressure is enabled
        test = speedTrial.nextTest(Pressure.getPressureEnabled());

        //Get a new phrase list
        phrasesList = test.getPhrases();

        //Reset the phrase counter
        phraseCounter = 0;

        // create a countdown before trial begins
        new CountDownTimer(6000, 1000) {

            //Get the countdown text box for later manipulation
            TextView countdownText = (TextView) findViewById(R.id.countdown);

            //Display the countdown on screen
            public void onTick(long millisUntilFinished) {

                //Update the countdown text box
                changeText(countdownText, enabledText() + millisUntilFinished / 1000);

                // force keyboard display
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

            }

            //Remove the countdown number when countdown has finished
            public void onFinish() {

                //Show whether pressure is enabled
                changeText(countdownText, enabledText());

                //Display a new phrase
                newPhrase();
                trialStarted = true;

            }
        }.start();


    }

    //Return whether pressure is enabled or not
    static String enabledText() {
        if (Pressure.getPressureEnabled()) return "Pressure test ";
        else return "Standard test ";
    }

    //Display a new phrase on screen
    void newPhrase() {
        //If there are phrases left to display then display them
        if (phraseCounter < phrasesList.size()) {
            changeText(phraseText, phrasesList.get(phraseCounter).toString());
            startTime = System.currentTimeMillis();
        }

    }

    //Set up listeners for each on screen button
    void setListeners() {

        // create the next trial button
        nextTrialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //If the trial is complete then save the file
                if (speedTrial.trialComplete()) {
                    editText.setEnabled(false);
                    submitButton.setVisibility(View.INVISIBLE);
                    nextTrialButton.setVisibility(View.INVISIBLE);

                    // write full stats file for test subject
                    speedTrial.exportToFile();

                    // go back to NameInput activity
                    finish();
                    return;
                }

                //toggle pressure keyboard input
                Pressure.setPressureEnabled(!Pressure.getPressureEnabled());

                //Move to next test
                secondTest();

            }
        });

        //Create the submit button
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Finish the trial if all phrases have been shown
                if (phraseCounter == phrasesList.size()) {
                    finishTrial();
                } else {

                    //Show the next phrase on screen
                    String text = phraseText.getText().toString();
                    text = text.trim();
                    String input = editText.getText().toString();

                    //If entered phrase is at least 2/3 of the input string then save time taken to complete
                    if (input.length() > ((text.length() / 3) * 2)) {
                        setDuration(startTime, phrasesList.get(phraseCounter), input);

                        //If there are phrases left to show then show next phrase
                        if (phraseCounter < phrasesList.size() - 1) {
                            phraseCounter++;
                            newPhrase();
                        } else {
                            finishTrial();
                        }

                    } else {
                        // set text colour to red to indicate more input needed
                        editText.setTextColor(Color.RED);

                        // change text colour back after 2 seconds
                        new CountDownTimer(2000, 2000) {

                            //CountdownTimer requires this function
                            public void onTick(long millisUntilFinished) {
                            }

                            //Reset text colour
                            public void onFinish() {
                                editText.setTextColor(Color.BLACK);
                            }
                        }.start();
                    }

                }
            }

        });

    }

    //Finish the trial
    void finishTrial() {

        //Show which test has just been completed (standard or pressure)
        String testNumber = "First";
        if (speedTrial.trialComplete()) testNumber = "Second";
        changeText(phraseText, testNumber + " test complete");

        //Remove the keyboard from the screen
        editText.setEnabled(false);
        editText.setEnabled(true);

        //Show the button to move to next trial or next participant
        submitButton.setVisibility(View.INVISIBLE);
        nextTrialButton.setVisibility(View.VISIBLE);

        // write out stats for given test
        test.exportToFile();

    }

    //Save the time taken to complete the given phrase
    void setDuration(long startTime, AssignedPhrase phrase, String input) {
        phrase.setStats((float) (System.currentTimeMillis() - startTime), 1, input);

        //Remove all previously inputted text
        changeText(editText, "");

    }

    //Change the text of any given TextView
    void changeText(TextView textView, String changedText) {
        textView.setText(changedText);
    }

    //Show the options menu
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Reset the trial if options button pressed
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Code to create dialog from: http://stackoverflow.com/a/13511580/3978772
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Reset trial");
        builder.setNeutralButton("Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

        return super.onOptionsItemSelected(item);
    }

    public static boolean getTrialStarted() {
        return trialStarted;
    }

}

