package mbsd.pressureproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.Intent;
import android.util.Log;

import java.util.Random;

public class NameInput extends AppCompatActivity {

    private static boolean onScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Show that the NameInput screen is currently open
        onScreen = true;

        //Set pressure to off for name input
        Pressure.setPressureEnabled(false);

        setContentView(R.layout.sample_name_input);
        createSubmitButton();


    }

    //Create a button to submit the user's name
    private void createSubmitButton() {

        //Set up name submit button
        Button nameSubmitButton = (Button) findViewById(R.id.submitButton);

        //Save the name and start a trial when name submit button is pressed
        nameSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText nameText = (EditText) findViewById(R.id.nameText);
                String name = nameText.getText().toString();

                //Randomly choose standard or pressure input
                Random rand = new Random();
                Pressure.setPressureEnabled(rand.nextBoolean());

                // call MainActivity
                goMainActivity(name);

            }
        });
    }

    //Called when a trial is complete and the name page is shown again
    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent i) {

        setContentView(R.layout.sample_name_input);

        createSubmitButton();

    }

    //Switch to the trial screen
    private void goMainActivity(String name) {

        // call MainActivity
        Intent i = new Intent(getApplicationContext(), MainActivity.class);

        // pass MainActivity participant name
        i.putExtra("participantName", name);

        onScreen = false;

        // await return of MainActivity
        startActivityForResult(i, 0);

    }

    //Return whether name page is currently shown or not
    public static boolean isOnScreen() {
        return onScreen;
    }
}