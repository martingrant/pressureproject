package mbsd.pressureproject;

public class Pressure {

    static protected int pressureValue = 0;
    static protected boolean pressureEnabled;
    static protected int pollFreq = 10;

    public static void setPressureValue(int i) {
        pressureValue=i;
    }

    public static void setPressureEnabled(boolean b) {
        pressureEnabled = b;
    }

    public static int getPressureValue(){
        return pressureValue;
    }

    public static int getPollFreq(){
        return pollFreq;
    }

    public static boolean getPressureEnabled(){
        return  pressureEnabled;
    }

}
