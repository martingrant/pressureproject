package mbsd.pressureproject;

/**
 * Created by Stephen Standaloft on 15/11/2015.
 */

import java.io.*;
import java.util.*;

class PhraseGenerator {

    // reads in phrases and stores them in an ArrayList of Phrases

    private ArrayList<Phrase> wordPhrase; // ArrayList of word phrases
    private ArrayList<Phrase> titlePhrase; //ArrayList of title case word phrase
    private ArrayList<Phrase> websitePhrase; // ArrayList of websites

    private static final int NUM_INPUT_PHRASES = 500;  // number of input phrases
    private static final int NUM_INPUT_WEBSITES = 100;  // number of input websites

    private Random randGen; // random number generator

    protected PhraseGenerator(File phrases, File websites) throws IOException {

        wordPhrase=new ArrayList<>(PhraseGenerator.NUM_INPUT_PHRASES);
        titlePhrase=new ArrayList<>(PhraseGenerator.NUM_INPUT_PHRASES);
        websitePhrase=new ArrayList<>(PhraseGenerator.NUM_INPUT_WEBSITES);

        randGen = new Random();

        // read in phrase set from file
        BufferedReader br = new BufferedReader(new FileReader(phrases));

        String line;
        while((line=br.readLine())!=null) {

            Phrase p = new Phrase(line);

            wordPhrase.add(p); // make word phrase set
            titlePhrase.add(p.titleCase()); // add to title case phrase set
        }

        // read in websites from file
        br = new BufferedReader(new FileReader(websites));

        while((line=br.readLine())!=null) {

            Phrase p = new Phrase(line);

            websitePhrase.add(p); // add to website phrase set
        }
    }

    protected Phrase getAccNum() {
        // generates a random account number in the following format:
        // sort code: xx-xx-xx account number: xxxxxxxx

        String sort = "Sort code: ";
        String acc = "account number: ";

        int digit = 0;
        for (int i = 0; i < 6; i++) {
            digit = randGen.nextInt(10); // generate digit ranging from 0..9
            sort+=digit;
            if((i+1)%2==0&&(i+1)<6) sort+="-"; // insert a "-" between every second digit
        }

        for (int i = 0; i < 8; i++) {
            digit = randGen.nextInt(10); // generate digit ranging from 0..9
            acc+=digit;
        }

        return new Phrase(sort+" "+acc);
    }

    protected Phrase getRandomPhrase() {
        // generates a random phrase consisting of 4 words of 4 random characters

        String type = "luds"; // lowerCase, upperCase, digit, symbol
        String symb = "!@#$/^&*()-\'\":;,"; // symbol set available on keyboard views
        String randPhr = "";
        char next = ' ';

        // create 4 words with 4 random characters in
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {

                // choose randomally what the type of char to add next should be
                next = type.charAt(randGen.nextInt(type.length()));

                if (next == 'l')
                    randPhr+=(char)(randGen.nextInt(26)+'a'); // 26 chars from 'a'
                else if (next=='u')
                    randPhr+=(char)(randGen.nextInt(26)+'A'); // 26 chars from 'A'
                else if (next=='d')
                    randPhr+=(char)(randGen.nextInt(10)+'0'); // 10 chars from '0'
                else
                    //randPhr+=(char)(randGen.nextInt(11)+'!'); // 11 chars from '!'
                    randPhr+=symb.charAt(randGen.nextInt(symb.length())); // choose random symbol on custom keyboard
            }
            randPhr+=" "; // space between words
        }
        return new Phrase(randPhr);
    }


    protected Phrase getWordPhrase() {
        // returns a randomally selected word phrase
        int randPos = randGen.nextInt(wordPhrase.size()-1);
        return wordPhrase.get(randPos);
    }

    protected Phrase getNextWordPhrase(Phrase p) {
        // returns next word phrase after p
        int pPos = wordPhrase.indexOf(p);
        return wordPhrase.get((pPos+1)%wordPhrase.size());
    }

    protected Phrase getTitlePhrase() {
        // returns a randomally selected title phrase with title csae
        int randPos = randGen.nextInt(titlePhrase.size() - 1);
        return titlePhrase.get(randPos);
    }

    protected Phrase getNextTitlePhrase(Phrase p) {
        // returns next title case phrase after p
        int pPos = titlePhrase.indexOf(p);
        return titlePhrase.get((pPos+1)%titlePhrase.size());
    }

    protected Phrase getWebsite() {
        // returns a randomally selected word phrase
        int randPos = randGen.nextInt(websitePhrase.size() - 1);
        return websitePhrase.get(randPos);
    }

    protected Phrase getNextWebsite(Phrase p) {
        // returns next account number phrase after p
        int pPos = websitePhrase.indexOf(p);
        return websitePhrase.get((pPos+1)%websitePhrase.size());
    }

    public String toString() {
        // prints out entire phraseList, one Phrase per line
        String out = "";
        for (Phrase p : wordPhrase) {
            out+=p + " \n";
        }
        return out;
    }

} //PhraseGenerator