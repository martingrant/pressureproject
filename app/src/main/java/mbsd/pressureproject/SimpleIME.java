package mbsd.pressureproject;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;
import java.util.TimerTask;
import android.os.Vibrator;

public class SimpleIME extends InputMethodService
        implements KeyboardView.OnKeyboardActionListener {

    // Keyboard layouts and view
    public Keyboard normalLayout;
    public Keyboard keyboardSymbolsLayout;
    //public Keyboard keyboardSymbolsLayout2; -- This extra symbols layout is currently unused.
    public KeyboardView keyboardView;

    // Pressure sensor resources
    private ShakeDeviceCS6 shake;
    private boolean connected = false;
    private Handler handler = new Handler();

    // Key codes to switch keyboard layouts
    final int symbols_shift = 55001;
    final int symbols = 55000;
    final int qwerty = 55002;

    // Boolean flag for switching caps lock on/off
    private boolean caps = false;

    // Vibrator instance to provide feedback when pressure levels are reached
    Vibrator vibrate;

    @Override
    public View onCreateInputView() {
        initPressureSensor();

        // Create keyboard layouts
        normalLayout = new Keyboard(this, R.xml.qwerty);
        keyboardSymbolsLayout = new Keyboard(this, R.xml.symbols);
        //keyboardSymbolsLayout2 = new Keyboard(this, R.xml.symbols_shift); -- This extra symbols layout is currently unused.

        // Create and setup the keyboard view
        keyboardView = (KeyboardView) getLayoutInflater().inflate(R.layout.keyboard, null);
        keyboardView.setKeyboard(normalLayout);
        keyboardView.setOnKeyboardActionListener(this);

        // Init the vibrator service
        vibrate = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        return keyboardView;
    }

    void initPressureSensor() {
        shake = new ShakeDeviceCS6();

        // Open connection to the pressure sensor
        if (!connected) {
            connected = shake.openConnection("00:50:C2:A1:D2:01");
        }

        // Call a thread to run continuously to read from the pressure sensor
        if (connected) {
            handler.post(new UpdateSHAKE());
        }
    }

    private class UpdateSHAKE extends TimerTask {

        // Store the current and previous keyboard types
        private int keyboardType = 0;
        private int previousKeyboard = -1;

        @Override
        public void run() {
            // Only run the following code if the phone has a connection to the pressure sensor
            if (connected) {
                // Store the value from the sensor
                int sensorValue = (int)shake.FSR0();
                Pressure.setPressureValue(sensorValue);

                // Only run the following code if the app is running a pressure test
                if (Pressure.getPressureEnabled()) {

                    // Connect the keyboard view to this custom IME's events
                    keyboardView.setOnKeyboardActionListener(SimpleIME.this);

                    // Get the pressure value
                    int pressureValue = Pressure.getPressureValue();

                    // If the pressure value is > 0 and < 5, use the normal keyboard layout, turn caps off
                    if (pressureValue > 0 && pressureValue < 5)
                    {
                        keyboardView.setKeyboard(normalLayout);
                        keyboardType = 0;
                        caps = false;
                    }
                    else if (pressureValue >= 8 && pressureValue < 15) // If the pressure value is >= 8 and < 15, use the normal layout but set the caps boolean to true
                    {
                        keyboardView.setKeyboard(normalLayout);
                        keyboardType = 1;
                        caps = true;
                        keyboardView.invalidateAllKeys();
                    }
                    else if (pressureValue >= 17 && pressureValue < 100) // If the pressure value is >= 17 use the symbols keyboard layout
                    {
                        keyboardView.setKeyboard(keyboardSymbolsLayout);
                        keyboardType = 2;
                        caps = false;
                    }

                    normalLayout.setShifted(caps);

                    // Vibrate the phone if the keyboard layout has changed
                    if (keyboardType != previousKeyboard){
                        vibrate.vibrate(5);
                        previousKeyboard = keyboardType;
                    }
                }
                // Repost listener thread to listen for pressure changes
                handler.postDelayed(this, Pressure.getPollFreq());
            }
        }
    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        // Change the keyboard layouts using on-screen keys when not conducting a pressure test
        switch (primaryCode) {
            case symbols:
                if (!Pressure.getPressureEnabled())
                    keyboardView.setKeyboard(keyboardSymbolsLayout);
                break;
            case qwerty:
                if (!Pressure.getPressureEnabled())
                    keyboardView.setKeyboard(normalLayout);
                break;
        }
    }

    @Override
    public void onPress(int primaryCode) {

        // Leave this function if the trial has not yet started
        if (!MainActivity.getTrialStarted() && !NameInput.isOnScreen())
            return;

        InputConnection inputConnection = getCurrentInputConnection();

        // Switch based on primaryCode which is the code from XML
        switch (primaryCode) {
            case Keyboard.KEYCODE_DELETE:
                inputConnection.deleteSurroundingText(1, 0);
                break;

            // Capatilise (or uncapitlise) the keyboard when the shift key is pressed
            case Keyboard.KEYCODE_SHIFT:
                if (!Pressure.getPressureEnabled()) {
                    caps = !caps;
                    normalLayout.setShifted(caps);
                    keyboardView.invalidateAllKeys();
                }
                break;

            case Keyboard.KEYCODE_DONE:
                inputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;

            // Ignore presses on the symbols layout button
            case symbols:
                break;

            // Ignore presses on the next symbols button (second symbols layout)
            case symbols_shift:
                break;

            // Ignore presses on qwerty layout button
            case qwerty:
                break;

            // Capitilise a character if shift key is on
            default:
                char code = (char) primaryCode;
                if (Character.isLetter(code) && caps) {
                    code = Character.toUpperCase(code);
                }

                inputConnection.commitText(String.valueOf(code), 1);

                // Turn off caps and show the normal (uncapitalised) layout again
                caps = false;
                normalLayout.setShifted(false);
                keyboardView.invalidateAllKeys();
        }
    }

    @Override
    public void onRelease(int primaryCode) {
    }

    @Override
    public void onText(CharSequence text) {
    }

    @Override
    public void swipeDown() {
    }

    @Override
    public void swipeLeft() {
    }

    @Override
    public void swipeRight() {
    }

    @Override
    public void swipeUp() {
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Do not listen for volume changes
        return (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        // Do not listen for volume changes
        return (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // Do not listen for volume changes
        return (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP);
    }
}
