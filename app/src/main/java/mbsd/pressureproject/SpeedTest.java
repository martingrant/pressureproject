package mbsd.pressureproject;

/**
 * Created by Stephen Standaloft on 15/11/2015.
 */

import android.os.Environment;
import android.util.Log;

import java.io.*;
import java.util.*;
import java.text.*;

class SpeedTest {

    // assigns phrases and records stats for a given input method test
    // write phrases and stats to a file for later analysis

    private ArrayList<AssignedPhrase> assigned; // list of phrases assigned to test subject

    private String participantName; // participant name
    private boolean pressureInput; // whether pressure input being used
    private char pressureFlag; // pressure flag for phrase/stats output

    private static final int NUM_0_PHRASES = 3; // number of standard phrases assigned
    private static final int NUM_1_PHRASES = 5; // number of title case phrases assigned
    private static final int NUM_2_PHRASES = 5; // number of acc nums assigned
    private static final int NUM_3_PHRASES = 5; // number of websites assigned
    private static final int NUM_4_PHRASES = 5; // number of random phrases assigned

    private static final int NUM_PHRASES = NUM_0_PHRASES+NUM_1_PHRASES
            +NUM_2_PHRASES+NUM_3_PHRASES
            +NUM_4_PHRASES; // number of phrases assigned

    protected SpeedTest(boolean usePressure,String partName) {

        participantName = partName;
        pressureInput = usePressure;
        pressureFlag = pressureInput ? 'P' : 'S';

        assigned = new ArrayList<>(SpeedTest.NUM_PHRASES);

        setPhrases(assigned);
    }

    private void setPhrases(ArrayList<AssignedPhrase> assigned) {

        // adds phrases to the SpeedTest's assigned phrase list using PhraseGenerator
        // number of phrases taken from phrase set x = NUM_x_PHRASES
        // copies phrase into an AssignedPhrase object to prevent aliasing

        PhraseGenerator phraseGen = SpeedTrial.getPhraseGenerator();
        Phrase phrase;

        for (int i = 0; i < NUM_0_PHRASES; i++) {

            phrase=phraseGen.getWordPhrase(); // get a lower case phrase from the phrase pool

            // make sure we've not already taken that phrase from the pool
            while (assigned.contains(phrase))
                phraseGen.getNextWordPhrase(phrase); // if we are get the next phrase
            assigned.add(new AssignedPhrase(phrase)); // add phrase to assigned phrases
        }

        for (int i = 0; i < NUM_1_PHRASES; i++) {

            phrase=phraseGen.getTitlePhrase(); // get a title case phrase from the phrase pool

            // make sure we've not already taken that phrase from the pool
            while (assigned.contains(phrase))
                phrase=phraseGen.getNextTitlePhrase(phrase); // if we are get the next phrase
            assigned.add(new AssignedPhrase(phrase)); // add phrase to assigned phrases
        }

        for (int i = 0; i < NUM_2_PHRASES; i++) {

            phrase=phraseGen.getAccNum(); // create a randomally generated acc num
            assigned.add(new AssignedPhrase(phrase)); // add phrase to assigned phrases
        }

        for (int i = 0; i < NUM_3_PHRASES; i++) {

            phrase=phraseGen.getWebsite(); // get a website from the website pool
            // make sure we've not already taken that website from the pool
            while (assigned.contains(phrase))
                phrase=phraseGen.getNextWebsite(phrase); // if we are get the next website
            assigned.add(new AssignedPhrase(phrase)); // add website to assigned phrases
        }

        for (int i = 0; i < NUM_4_PHRASES; i++) {

            phrase=phraseGen.getRandomPhrase(); // create a randomally generated phrase
            assigned.add(new AssignedPhrase(phrase)); // add phrase to assigned phrases
        }

    }

    protected ArrayList<AssignedPhrase> getPhrases() {
        // returns the assigned phrase list
        return assigned;
    }

    protected String exportPhrases() {
        // returns the assigned phrases in string format, one per line
        String out = "";
        int count = 0;

        for (AssignedPhrase ap : assigned) {

            out+=participantName+ " "+pressureFlag +" "+ count++ +" "+ ap + "\n";
        }

        return out;
    }


    protected String exportStats() {

        // returns the stats for this test as a single string
        // but without table headers

        String out = "";

        // output stats for phrases type in with standard keyboard
        for (int i = 0; i < assigned.size(); i++) {

            // output phrase number and stats for phrase
            out+=participantName +","+pressureFlag+","+ i +","+assigned.get(i).getStats()+"\n";
        }

        return out;
    }


    protected void exportToFile() {
        // writes the stats/phrases for this test as a single string
        try {
            File stats = new File(Environment.getExternalStorageDirectory(),
                    "pressureproject/subject " + participantName +" "+ pressureFlag+ ".csv");

            FileOutputStream fos = new FileOutputStream(stats);
            OutputStreamWriter osw = new OutputStreamWriter(fos);

            // output stats with headers
            osw.write(SpeedTrial.STATS_TABLE_HEADER+exportStats());
            // new line seperators
            osw.write("\n\n\n");
            // output phrases
            osw.write(exportPhrases());

            osw.close();
        } catch (IOException ie) { System.out.println("File error"); }
    }

    public String toString() {
        // returns all stats/phrases for this SpeedTest
        return SpeedTrial.STATS_TABLE_HEADER+exportStats()+"\n\n\n"+exportPhrases();
    }
} //SpeedTest